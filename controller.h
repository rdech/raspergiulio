#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(float p_k, QObject *parent = nullptr);

signals:
    void signalResult(QByteArray line, bool last);
public slots:
    void compute(float p_xa, float p_ya, float p_xb, float p_yb, bool last);
private:
    float m_k; //max distance defined in the constructor.
};

#endif // CONTROLLER_H
