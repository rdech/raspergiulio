#include <QCoreApplication>
#include "initializer.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    float k=atof(argv[1]);
    //float k=1.2;
    Initializer in("input", "output", k);
    QObject::connect(&in, &Initializer::terminated, &a, &QCoreApplication::quit, Qt::QueuedConnection);
    in.init();
    return a.exec();
}
