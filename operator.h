#ifndef OPERATOR_H
#define OPERATOR_H

#include <QObject>
#include <QFile>

class Operator : public QObject
{
    Q_OBJECT
public:
    explicit Operator(QFile *p_input, QFile *p_output, QObject *parent = nullptr);

signals:
    void compute(float xa, float ya, float xb, float yb, bool last);
    void closing();
public slots:
    void start();
    void print(QByteArray line, bool last);
private:
    QFile *m_input=nullptr;
    QFile *m_output=nullptr;
    bool flag;
};

#endif // OPERATOR_H
