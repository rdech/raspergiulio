#include "controller.h"
#include <cmath>
#include <cstdio>

Controller::Controller(float p_k, QObject *parent) :
    QObject(parent),
    m_k(p_k)
{

}

void Controller::compute(float p_xa, float p_ya, float p_xb, float p_yb, bool last)
{
    QByteArray ris="";
    char res[100];
    sprintf(res, "%.2f,%.2f", p_xa, p_ya);
    ris.append(res);
    float coeffx, coeffy, d;
    float l_xc, l_yc;
    int i;
    coeffx=p_xa<p_xb?1:(p_xa>p_xb?-1:0);
    coeffy=p_ya<p_yb?1:(p_ya>p_yb?-1:0);
    coeffx*=coeffy==0?1:0.5;
    coeffy*=coeffx==0?1:0.866;
    d=sqrt(pow((p_xa-p_xb),2)+pow((p_ya-p_yb),2));
    for(i=2; d>m_k; i++){
        d=d/i;
    }
    for(i--; i>1; i--){
        l_xc=p_xa+(coeffx*d);
        l_yc=p_ya+(coeffy*d);
        sprintf(res, " %.2f,%.2f", l_xc, l_yc);
        ris.append(res);
        p_xa=l_xc;
        p_ya=l_yc;
    }
    sprintf(res, " %.2f,%.2f", p_xb, p_yb);
    ris.append(res);
    emit signalResult(ris, last);
}
