#ifndef INITIALIZER_H
#define INITIALIZER_H

#include <QObject>
#include <QFile>
#include <QString>
#include "controller.h"
#include "operator.h"

class Initializer : public QObject
{
    Q_OBJECT
public:
    explicit Initializer(QString p_input, QString p_output, float p_distance, QObject *parent = nullptr);
    void init();
signals:
    void signalStart();
    void terminated();
public slots:
    void closing();
private:
    Operator *m_operator = nullptr;
    Controller *m_controller = nullptr;
    QFile *m_input;
    QFile *m_output;
    float m_distance;
};

#endif // INITIALIZER_H
