#include "initializer.h"
#include <QCoreApplication>

Initializer::Initializer(QString p_input, QString p_output, float p_distance, QObject *parent) :
    QObject(parent),
    m_distance(p_distance)
{
    m_input=new QFile(p_input);
    m_output=new QFile(p_output);
    if (!m_input->open(QIODevice::ReadOnly | QIODevice::Text)) return;
    if (!m_output->open(QIODevice::WriteOnly | QIODevice::Text)) return;
}

void Initializer::init()
{
    Operator *m_operator=new Operator(m_input, m_output);
    Controller *m_controller=new Controller(m_distance);
    connect(this, &Initializer::signalStart, m_operator, &Operator::start);
    connect(m_controller, &Controller::signalResult, m_operator, &Operator::print);
    connect(m_operator, &Operator::compute, m_controller, &Controller::compute);
    connect(m_operator, &Operator::closing, this, &Initializer::closing);
    emit signalStart();
}

void Initializer::closing()
{
    m_output->close();
    m_input->close();
//    free(m_input);
//    free(m_output);
//    free(m_controller);
//    free(m_operator);
    printf("terminated\n");
    emit terminated();
}
