#include "operator.h"
#include <QTextStream>

Operator::Operator(QFile *p_input, QFile *p_output, QObject *parent) :
    QObject(parent),
    m_input(p_input),
    m_output(p_output)
{
    flag=false;
}

void Operator::start()
{
    float xa, ya, xb, yb;
    QString line;
    while(!m_input->atEnd()){
        line=m_input->readLine();
        QStringList pair=line.split(" ");
        QString alist=pair.at(0);
        QStringList a=alist.split(",");
        QString val=a.at(0);
        xa=val.toFloat();
        val=a.at(1);
        ya=val.toFloat();
        QString blist=pair.at(1);
        QStringList b=blist.split(",");
        val=b.at(0);
        xb=val.toFloat();
        val=b.at(1);
        yb=val.toFloat();
        if(m_input->atEnd()) flag=true;
        emit compute(xa, ya, xb, yb, flag);
    }
    return;
}

void Operator::print(QByteArray line, bool last)
{
    m_output->write(line+"\n");
    if(last){
        emit closing();
    }
}
